from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import SessionNotCreatedException as wrong_driver
import time
from threading import Thread
import json
import sqlite3

drugs = []
page_number = 1
conn = sqlite3.connect('drugs_database.db')
curs = conn.cursor()

def get_info_from_page():
	global browser
	time.sleep(1)
	one = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[2]')
	find_needed(one)
	browser.back()
	two = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[3]')
	find_needed(two)
	browser.back()
	three = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[4]')
	find_needed(three)
	browser.back()
	four = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[5]')
	find_needed(four)
	browser.back()
	five = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[6]')
	find_needed(five)
	browser.back()
	six = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[7]')
	find_needed(six)
	browser.back()
	seven = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[8]')
	find_needed(seven)
	browser.back()
	eight = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[9]')
	find_needed(eight)
	browser.back()
	nine = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[10]')
	find_needed(nine)
	browser.back()
	ten = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr"]/tbody/tr[11]')
	find_needed(ten)
	browser.back()

def find_needed(number):
    global drugs, browser, conn, curs
    number.click()
    try:
        pharma_tarep_group = browser.find_element_by_xpath('//*[@id="ctl00_plate_grFTG"]/tbody')
        pharma_tarep_group = pharma_tarep_group.text
        pharma_tarep_group = pharma_tarep_group.replace('Фармако-терапевтическая группа\n', '')
    except Exception as e:
        pharma_tarep_group = False
    
    try:
        torg_naimen = browser.find_element_by_xpath('//*[@id="ctl00_plate_TradeNmR"]')
        torg_naimen = torg_naimen.get_attribute("value")
    except Exception as e:
        torg_naimen = False
    
    try:
        veshestvo = browser.find_element_by_xpath('//*[@id="ctl00_plate_gr_fs"]/tbody/tr[2]/td[1]')
        veshestvo = veshestvo.text
    except Exception as e:
        veshestvo = False
    print('Добавил: ', torg_naimen)
    curs.execute("INSERT INTO drugs(product_name, pharma_terapevt_group, substance) VALUES (?, ?, ?)", (torg_naimen, pharma_tarep_group, veshestvo))
    conn.commit() # все заносится и сохраняется в БД
    drugs.append((torg_naimen, pharma_tarep_group, veshestvo))
def parse():
	global drugs, browser, page_number

	while page_number != 1032: #1032
		s1 = time.time()
		browser.get("https://grls.rosminzdrav.ru/GRLS.aspx?RegNumber=&MnnR=&lf=&TradeNmR=&OwnerName=&MnfOrg=&MnfOrgCountry=%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F&isfs=0&isND=0&order=RegDate&orderType=desc&RegType=1&pageSize=10&pageNum={}".format(page_number))
		print("Зашел на страницу {}".format(page_number))
		get_info_from_page()
		print("Обработал страницу {}".format(page_number))
		s2 = time.time()
		time_one_page = s2-s1
		print('Обработка страницы занимает: ', time_one_page, 'секунд')
		time_to_end = (1031 - page_number) * time_one_page
		print('Ожидаемое время до конца парсинга примерно: ', int(time_to_end / 60), 'минут или',  float(time_to_end / 3600), 'часов')
		page_number += 1

if __name__ == '__main__':
	
	while page_number != 1032:
		try:
			browser = webdriver.Chrome(executable_path=r".\chromedriver.exe")
			parse()
		except wrong_driver:
			print('ОШИБКА! Убедитесь, что ваш GOOGLE CHROME браузер обновлен до 83 версии, так как только эта версия совместима с драйвером')
		except Exception as e:
			print("Споткнулся о капчу")
			browser.close()
	with open('data.json', 'w') as f:
		json.dump(drugs, f, ensure_ascii=False)
	conn.close()

# переключение страниц с помощью последнего параметра ссылки

# 10289 элементов должно быть
# //*[@id="aspnetForm"]/div[3]/div[3]/table[3]/tbody/tr/td[2]
# //*[@id="aspnetForm"]/div[3]/div[3]/table[3]/tbody/tr/td[9]
# th1, th2, th3 = Thread(target=self.fonbet), Thread(target=self.olimp), Thread(target=self.marathon)
#        th1.start(), th2.start(), th3.start()
#        th1.join(), th2.join(), th3.join()
