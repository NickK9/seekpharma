import sqlite3

conn = sqlite3.connect('drugs_database.db')
curs = conn.cursor()
"""Это сбрасывает таблицу и создает новую"""
try:
    curs.execute('''DROP TABLE drugs''')
    curs.execute('''CREATE TABLE drugs(id INTEGER PRIMARY KEY AUTOINCREMENT,
product_name VARCHAR(1000), 
pharma_terapevt_group VARCHAR(1000),
substance VARCHAR(1000)
)
''')
except:
    curs.execute('''CREATE TABLE drugs(id INTEGER PRIMARY KEY AUTOINCREMENT,
product_name VARCHAR(1000), 
pharma_terapevt_group VARCHAR(1000),
substance VARCHAR(1000)
)
''')

conn.close()